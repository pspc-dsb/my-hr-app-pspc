var firebase = require("firebase");
var admin = require("firebase-admin");
var serviceAccount = require("./serviceAccountKey.json");

admin.initializeApp({
    credential: admin.credential.cert(serviceAccount),
    databaseURL: "https://hr-mobile-app-pwgsc.firebaseio.com"
});


const express = require('express');

// Get the Auth service for the default app
//var defaultAuth = firebase.auth();


db= admin.firestore();


// Get entire collection
async function getCollection() {
    const snapshot = await db.collection('users').get()
    snapshot.forEach(doc => {
    });
    
    return snapshot.docs.map(doc => doc.data());
}

var collection=[];

var promises= [];

function subscribeUUIDs(docs, topic) {
    let uuids = []; 
    for (var i=0; i < docs.length; i++) {
	var uuid = docs[i].token;
	uuids.push(uuid);
    }
    admin.messaging().subscribeToTopic(uuids,topic).then(function(response) {
	if (response.errors.length > 0) {
	    let e = response.errors;
	    e.forEach(function(item) {
		let doc = docs[item.index];
		promises.push(db.collection('users').doc(doc.user).delete());
	    });
	    if (promises.length > 0)
		Promise.all(promises).then((result) => {
		    console.log("Cleaned up!");
		    //		process.exit();
		})
		.catch(error => {
		    console.log(`Error in promises ${error}`);
		    //		process.exit();
		});				       
	}
	else {
	    console.log('Successfully subscribed to ', topic,':', uuids, "with response: ", response);
	    console.log("");
	    //	    process.exit();
	}
    })
	.catch(function(error){
	    console.log('Error subscribing to topic:',error);
	});

}


function unsubscribeUUIDs(docs,topic){
    uuids = [];
    for (var i=0; i < docs.length; i++) {
	var uuid = docs[i].token;
	uuids.push(uuid);
    }

    admin.messaging().unsubscribeFromTopic(uuids,topic).then(function(response) {
	console.log('Successfully unsubscribed from ', topic,':', uuids, "with response: ", response);
	console.log('');
    })
	.catch(function(error){
	    console.log('Error unsubscribing from topic:',error);
	});

}
    

const topic_en = "notifications_en";
const topic_fr = "notifications_fr";

// First get entire current collection and subscribe

//getCollection().then((data) => {
//    
//    subscribeUUIDs(data,topic);
//
//});



// Turns out it does a foreach from empty state.




const observer = db.collection('users')
      .onSnapshot(querySnapshot => {
	  querySnapshot.docChanges().forEach(change => {
	      if (change.type === 'added') {
		 // console.log('New: ', change.doc.data());
		  var docs = [];
		  var doc = change.doc.data();
		  docs.push(doc);
		  if (doc.language === 'en')
		      subscribeUUIDs(docs,topic_en);
		  else
		      subscribeUUIDs(docs,topic_fr);
		      
	      }
	      if (change.type === 'modified') {
		  //console.log('Modified: ', change.doc.data());
		  var docs = [];
		  var doc = change.doc.data();
		  docs.push(doc);
		  if (doc.language === 'en') {
		      unsubscribeUUIDs(docs,topic_fr);
		      subscribeUUIDs(docs,topic_en);
		  }
		  else {
	              unsubscribeUUIDs(docs,topic_en);
		      subscribeUUIDs(docs,topic_fr);
		  }

	      }
	      if (change.type === 'removed') {
		  //console.log('Removed: ', change.doc.data());
		  var docs = [];
		  var doc = change.doc.data();
		  docs.push(doc);
		  if (doc.language === 'en')
		      unsubscribeUUIDs(docs,topic_en);
		  else
		      unsubscribeUUIDs(docs,topic_fr)
	      }
	  });
      });

