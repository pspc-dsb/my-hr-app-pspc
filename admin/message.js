var firebase = require("firebase");
var admin = require("firebase-admin");
var serviceAccount = require("./serviceAccountKey.json");
var fs = require("fs");
var datetime = new Date();




console.log(datetime.toISOString());

admin.initializeApp({
    credential: admin.credential.cert(serviceAccount),
    databaseURL: "https://hr-mobile-app-pwgsc.firebaseio.com"
});


var firestore = admin.firestore();

const topic_en = "notifications_en"
const topic_fr = "notifications_fr";

const webpush = {
    fcm_options : {
	link: "https://myhrapppspc.web.app/"
    }
};


var  content_en = {
    title: "This Is The Message Title",
    body: "This is the message body. More detail goes here.",
    icon: "https://myhrapppspc.web.app/logo-192.png",
    tag:  datetime.toISOString(),
    link: "https://myhrapppspc.web.app"

};

var content_fr = {
    title: "C'est le sujet du message",
    body:  "C'est le corps du message. Plus de détails ici.",
    icon:   "https://myhrapppspc.web.app/logo-192.png",
    tag:    datetime.toISOString(),
    link:  "https://myhrapppspc.web.app"
    
};

var message_en = {};
var message_fr = {};


message_fr.data = content_fr;
message_en.data = content_en;
message_fr.data.priority = "high";
message_en.data.priority = "high";

message_fr.topic = topic_fr;
message_en.topic = topic_en;

console.log("English:  ", message_en);
console.log("Francais: ", message_fr); 


var messages = [];
var promises = [];

messages.push(message_fr);
messages.push(message_en);


if (messages.length > 0) {
    messages.forEach(function(message) {
	promises.push(admin.messaging().send(message));
    });
    if (promises.length > 0)
	Promise.all(promises).then((response) =>{
	    console.log("Successfully sent messages:", response);
	    process.exit(0)
	})
	.catch((error) => {
	    console.log("Error sending messages:", error);
	    process.exit(1)
	});
}

