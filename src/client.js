import * as sapper from '@sapper/app';
import { notification } from './routes/_stores';

sapper.start({
	target: document.querySelector('#sapper')
});

const messaging = firebase.messaging();

/**
 * Returns a hash code for a string.
 * (Compatible to Java's String.hashCode())
 *
 * The hash code for a string object is computed as
 *     s[0]*31^(n-1) + s[1]*31^(n-2) + ... + s[n-1]
 * using number arithmetic, where s[i] is the i th character
 * of the given string, n is the length of the string,
 * and ^ indicates exponentiation.
 * (The hash value of the empty string is zero.)
 *
 * @param {string} s a string
 * @return {number} a hash code value for the given string.
 */  
const hashCode = function(s) {
	var h = 0, l = s.length, i = 0;
	if ( l > 0 )
	while (i < l)
		h = (h << 5) - h + s.charCodeAt(i++) | 0;
	return h;
};

function messageSeen(message) {
	let hashArrayStr = localStorage.getItem("seen_message_hashes");
	if (hashArrayStr === null) {
	hashArrayStr = [];
	}
	else {
	hashArray =JSON.parse(hashArrayStr);
	}
	let msgHash = hashCode(JSON.stringify(message));
	console.log("messageSeen:", message, hash);
	if (hashArray.indexOf(msgHash) === -1) {
	// not found before, store for later.
	hashArray.push(msgHash);
	localStorage.setItem("seen_message_hashes", JSON.stringify(hashArray));
	console.log("message not seen");
	return false
	}
	else {
	console.log("message seen");
	return true
	}
}

async function addMessage(message){
	let messages;
	const mostRecent = -5;  // negative value here
	if (message === null) {
	console.error("Null message received.");
	}
	else{
		console.log("message:", message);
		let msgstr = localStorage.getItem("firebase_messages");
		if (msgstr === null) {
			messages = [];
		}
		else {
			messages = JSON.parse(msgstr);
		}
		// Javascript optimization does a lot here, so let's write this as explanatory code.
		// Make sure we're not missing any messages, fetch from firestore
		let firestore_messages =await getRecentNotifications();
		console.log("firestore messages:", firestore_messages);
		let combined_messages=messages.concat(firestore_messages);
		combined_messages.push(message);
		// Removes duplicates and trims to the last 5
		let unique_messages = [... new Set(combined_messages)];
		let trimmed_messages = unique_messages.slice(mostRecent);
		console.log("messages: ", trimmed_messages);
		localStorage.setItem("firebase_messages", JSON.stringify(trimmed_messages));
		console.log("trimmed_messages:", trimmed_messages);
		notification.set(trimmed_messages[0]);
	}
}

	 
function getMessages(){
	let messages;
	let msgstr = localStorage.getItem("firebase_messages");
	if (msgstr === null) {
	   messages = [];
	}
	else {
		messages = JSON.parse(msgstr);
	}
	return messages;
}

function clearMessages() {
	let emptyarray = [];
	localStorage.setItem("firebase_messages",JSON.stringify(emptyarray));
	localStorage.setItem("seen_messag_hashes",JSON.stringify(emptyarray));
}


function getMessagingLanguage() {
	return window.localStorage.getItem('lang');
}


// Do we need this?
//
function setMessagingLanguage(lang) {
	if ((lang === 'en') || (lang === 'fr')){
	console.log("messaging language set to: ", lang);
	window.localStorage.setItem('lang', lang);
	}
	else {
	console.log("Unsupported language:", lang);
	}   
}

function setUser(user){
	window.localStorage.setItem('user',JSON.stringify(user));
}

function getUser(){
	return JSON.parse(window.localStorage.getItem('user'));
}

//
// End of localstorage helper functions
//


firebase.auth().signInAnonymously().catch(function(error) {
	// Handle Errors here.
	var errorCode = error.code;
	var errorMessage = error.message;
	
	if (errorCode === 'auth/operation-not-allowed') {
	alert('You must enable Anonymous auth in the Firebase Console.');
	} else {
	console.error(error);
	}
});
	  



firebase.auth().onAuthStateChanged(function(user) {
	if (user) {
	// User is signed in.
	var uid = user.uid;
	setUser(user);
	console.log("AuthStateChanged");
	messaging.getToken().then((Token) => {
		sendTokenToServer(Token);
	});
	 // ...
	} else {
	// User is signed out.
	// ...
	}
	// ...
});

window.setMessagingTopic = function () {
//     console.log("set messaging topic");
	messaging.getToken().then((Token) => {
		sendTokenToServer(Token);
	});
}

// Send the Instance ID token your application server, so that it can:
// - send messages back to this app
// - subscribe/unsubscribe the token from topics
// - subscribe to the correct topic langauge
//
const db = firebase.firestore();


//
//  This function gets const MostRecent notfications for the topic from firebase firestore.
//  
async function getRecentNotifications() {
	const mostRecent = 5;
	var topic = "notifications_" + getMessagingLanguage();
	console.log("topic", topic);
	let result = await db.collection(topic)
	.orderBy("index", "desc")
		  .limit(mostRecent)
	.get();
	console.log("result", result);
	var docs = result.docs.map(doc => doc.data());
	console.log("docs", docs);
	return docs;
}




function sendTokenToServer(currentToken) {
	var lang = getMessagingLanguage();
	var currentUser = getUser();
//     console.log("lang:", lang);
//     console.log("currentUser:", currentUser);
//     console.log("currentToken:", currentToken);
	// TODO(developer): Send the current token to your server.
	if (currentUser && currentToken && lang) {

	var id=JSON.stringify(currentUser.uid);

	var content={};
	content.user = id;
	content.token = currentToken;
	content.language = lang;

	var contentStr = JSON.stringify(content);
	if (contentStr.localeCompare(window.localStorage.getItem('subscription_document')) === 0) {
		console.log("Registration document hasn't changed, not sending");
	}
	else {
		db.collection("users").doc(id).set(content).then(function() {
		console.log("Wrote document", content);
		window.localStorage.setItem('subscription_document', contentStr);
		}).catch(function(error) {
		console.error("Error writing document: ",content,", getting errror: ", error);
		});

	}

	}
	else {
	if (currentToken === null) { 
		console.log("Don't have current token yet, wait.");
	}
	if (currentUser=== null){
		console.log("Don't have current user yet, wait.");
	}
	if (lang === null) {
		console.log("Don't have current lang yet, wait.");
	}
	}		  
}



function requestPermission() {
	console.log('Requesting permission...');
	// [START request_permission]
	Notification.requestPermission().then((permission) => {
	if (permission === 'granted') {
		// TODO(developer): Retrieve an Instance ID token for use with FCM.
		// [START_EXCLUDE]
				// In many cases once an app has been granted notification permission,
		// it should update its UI reflecting this.
		initializeMessaging();
		// [END_EXCLUDE]
	} else {
		console.log('Unable to get permission to notify.');
	}
	});
	// [END request_permission]
}



document.addEventListener('DOMContentLoaded', function() {
	if (!Notification) {
	return;
	}
	if (Notification.permission !== 'granted') 
	requestPermission();
	return true  // See https://support.google.com/chrome/thread/2047906?hl=en
});

	/// hmmm

/*	  
navigator.serviceWorker.getRegistrations().then(function(registrations) {
	registrations.forEach(function(v) {
	if (v.active)
	if (v.active.scriptURL === "https://myhrapppspc.web.app/firebase-messaging-sw.js") {
		console.log('found my service worker')
		console.log(v);
//		      v.addEventListener('message', sw_message_handler);
	}
	
	});
});

*/



navigator.serviceWorker.addEventListener('message', sw_message_handler);

function sw_message_handler(event) {
	console.log("event:", typeof event, event);

	if (event.isTrusted === true) {   // only process truested events
	console.log("event has data field:",event.hasOwnProperty('data'));
	console.log("event.data has data field:",event.data.hasOwnProperty('data'));

	if (event.data.hasOwnProperty('data')) {  //strip everything but event.data.data, the core fields.
		let message  = event.data.data;
		if (event.data.isFirebaseMessaging === false) {
		// This is a background message, ensure it is "seen" by adding it to the hash list by calling messageSeen()
		console.log("background message:", message);
		
		if (messageSeen(message) === true) {
			console.log("background message seen before.");
		}
		console.log("firebase message received");
		}
		addMessage(message);
	}
	else {
		console.err("error in message event, missing data.data field:", event);
	}
	}
	else {
	console.error("event is not trusted!", event);
	}
	return  true 
}
	
//          const broadcastChannel = new BroadcastChannel('messages');
//          broadcastChannel.onmessage = function (message) {
//	      console.log("background message in main", message);
//	  };


	// Need a UI hook for this.
//
function deleteToken() {
	// Delete Instance ID token.
	// [START delete_token]
	messaging.getToken().then((currentToken) => {
	messaging.deleteToken(currentToken).then(() => {
		console.log('Token deleted.');
		// [START_EXCLUDE]
		// Once token is deleted update UI.
		initializeMessaging();
		// [END_EXCLUDE]
	}).catch((err) => {
		console.log('Unable to delete token. ', err);
	});
// [END delete_token]
	}).catch((err) => {
	console.log('Error retrieving Instance ID token. ', err);
	});
	
};

	 

// [START refresh_token]
// Callback fired if Instance ID token is updated.
messaging.onTokenRefresh(() => {
	messaging.getToken().then((refreshedToken) => {
	console.log('Token refreshed.');
	// Indicate that the new Instance ID token has not yet been sent to the
	// app server.
	// Send Instance ID token to app server.
	sendTokenToServer(refreshedToken);
	// [START_EXCLUDE]
	// Display new Instance ID token and clear UI of all previous messages.
	initializeMessaging();
	// [END_EXCLUDE]
	 }).catch((err) => {
	 console.log('Unable to retrieve refreshed token ', err);
	});
});
// [END refresh_token]



// [START receive_message]
// Handle incoming messages. Called when:
// - a message is received while the app has focus
// - the user clicks on an app notification created by a service worker
//   `messaging.setBackgroundMessageHandler` handler.


//Service worker message handler now handles these mesages.

//messaging.onMessage((payload) => {
//     console.log('Message received from firebase! ', payload);
//     addMessage(payload);
//     
// });
// [END receive_message]



function sortByKey(array, key) {
	
	return array.sort(function(a, b) {
	var x = a[key]; var y = b[key];
	return ((x < y) ? -1 : ((x > y) ? 1 : 0));
	});
}



function initializeMessaging() {
	// [START get_token]
	// Get Instance ID token. Initially this makes a network call, once retrieved
	// subsequent calls to getToken will return from cache.
	messaging.getToken().then((currentToken) => {
	if (currentToken) {
		console.log("initializeMessaging");
		sendTokenToServer(currentToken);
	} else {
		// Show permission request.
		console.log('No Instance ID token available. Request permission to generate one.');
		// Show permission UI.
				 }
	}).catch((err) => {
	console.log('An error occurred while retrieving token. ', err);
	});
	// [END get_token]
}
