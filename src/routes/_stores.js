import { writable } from 'svelte/store';

export const meta = writable({
    lang: null,
    altLang: null,
    slugs: null,
    path: null,
    translation: null
});

export const notification = writable(null);