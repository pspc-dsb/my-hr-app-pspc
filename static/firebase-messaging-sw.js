// Import and configure the Firebase SDK
// These scripts are made available when the app is served or deployed on Firebase Hosting
// If you do not serve/host your project using Firebase Hosting see https://firebase.google.com/docs/web/setup

importScripts('/firebase/firebase-app.js');
importScripts('/firebase/firebase-analytics.js');
importScripts('/firebase/firebase-messaging.js');
importScripts('/firebase/init.js');


/*
 // [START initialize_firebase_in_sw]
 // Give the service worker access to Firebase Messaging.
 // Note that you can only use Firebase Messaging here, other Firebase libraries
 // are not available in the service worker.
 importScripts('https://www.gstatic.com/firebasejs/7.19.1/firebase-app.js');
 importScripts('https://www.gstatic.com/firebasejs/7.19.1/firebase-messaging.js');

 // Initialize the Firebase app in the service worker by passing in
 // your app's Firebase config object.
 // https://firebase.google.com/docs/web/setup#config-object

const firebaseConfig = {
  apiKey: "AIzaSyCNmPFDHCjmDCKA7WEZiodumwKaBl3WJJ4",
  authDomain: "myhrapppspc.firebaseapp.com",
  databaseURL: "https://myhrapppspc.firebaseio.com",
  projectId: "myhrapppspc",
  storageBucket: "myhrapppspc.appspot.com",
  messagingSenderId: "662755527934",
  appId: "1:662755527934:web:e34f585aa6b78a1cf4a9af",
  measurementId: "G-54ECTGCVBL"
};





firebase.initializeApp(firebaseConfig);
*/





/*

/*
* Overrides push notification data, to avoid having 'notification' key and firebase blocking
* the message handler from being called

self.addEventListener('push', function (e) {
  // Skip if event is our own custom event
  if (e.custom) return;
  // Create a new event to dispatch
  var newEvent = new Event('push');
  newEvent.waitUntil = e.waitUntil.bind(e);
  newEvent.data = {
     json: function() {
         var newData = e.data.json();
         newData._notification = newData.notification;
         delete newData.notification;
         return newData;
     },
  };     
  newEvent.custom = true;          

  // Stop event propagation
  e.stopImmediatePropagation();

  // Dispatch the new wrapped event
  dispatchEvent(newEvent);
});

*/



// Retrieve an instance of Firebase Messaging so that it can handle background
// messages.
const messaging = firebase.messaging();

messaging.setBackgroundMessageHandler(function (payload) {

    // Customize notification here
    const notificationTitle = payload.data.title;
    const notificationOptions = {
	title: payload.data.title,
	body: payload.data.body,
	icon: payload.data.icon,
	tag:  payload.data.tag,
	data: payload.data
  };
    console.log("Notification options:", notificationOptions);   
  return self.registration.showNotification(notificationTitle,
					    notificationOptions);
});


/*
self.addEventListener('notificationclick', function(event) {
    console.log("Clicked:", event);
    event.waitUntil(clients.matchAll({
    type: "window",
    includeUncontrolled: true
    }).then(function (clientList) {	
    for (var i = 0; i < clientList.length; i++) {
            var client = clientList[i];
        console.log("Client: ", client);
            if (client.url === event.notification.data && 'focus' in client)
        return client.focus();
    }
    if (clients.openWindow)
            return clients.openWindow(event.notification.data);
    }));
});

*/

self.addEventListener('notificationclick', event => {
    console.log("Clicked", event);
    event.waitUntil(async function() {
	const allclients = await clients.matchAll({
	    type: "window",
	    includeUncontrolled: true
	});

	let myclient;
	let link = event.notification.data.link;
	for (const client of allclients) { 
            if (client.url.includes(link)) {
		client.focus();
		myclient = client;
		break;
	    }
	}
	if (!myclient)
	    if (clients.openWindow) {
		myclient = await clients.openWindow(link);
	    }
	if (myclient) {
	    let obj = JSON.parse(JSON.stringify(event.notification));
	    myclient.postMessage( {
		title: event.notification.data.title,
		body:  event.notification.data.body,
		icon:  event.notification.data.icon,
		tag:   event.notification.data.tag,
		link:  event.notification.data.link
	    });
	}
	return true;
    }());
});
