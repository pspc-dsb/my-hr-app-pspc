#!/bin/bash
git submodule add https://gitlab.com/pspc-dsb/my-hr-app-pspc-content-english.git src/routes/en
git submodule add https://gitlab.com/pspc-dsb/my-hr-app-pspc-content-french.git src/routes/fr
git submodule sync --recursive
